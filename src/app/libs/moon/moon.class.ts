import data from './data';
import * as moment from 'moment'
import * as lunarDays from 'lunardays'

export class Moon {

  static getInstance() {
    return new this;
  }

  getMoonData() {
    return data
      .trim()
      .split('->>')
      .map((s) => s.trim())
      .filter((s) => s.length)
      .map((s) => {
        const lines = s
          .split(/\n/)
          .map((s) => s.trim())
          .filter((s) => s.length);

        return {
          title: lines[0],
          content: lines.slice(1)
        }
      });
  }

  getMoonDayData(number: number) {
    return this.getMoonData()[number - 1];
  }

  getCurrentMoonDay() {
    // create some date
    const date = moment()
    // MSK
    const latitude = 56
    const longitude = 38
    // get all lunar days for date
    const days = lunarDays(date, latitude, longitude)

    const { number } = days
      .filter(({ start, end }) => date.isBetween(start, end))
    [0];

    return number;
  }

  getCurrentMoonDayData() {
    return this.getMoonDayData(this.getCurrentMoonDay());
  }

}
