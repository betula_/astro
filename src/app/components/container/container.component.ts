import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'x-container',
  template: `
    <div class="content">
      <ng-content></ng-content>
    </div>
  `,
  styleUrls: ['./container.component.scss']
})
export class ContainerComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
