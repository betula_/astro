import { Component, OnInit } from '@angular/core';
import { Moon } from '../../libs/moon/moon.class';

@Component({
  selector: 'x-moonday',
  template: `
    <b>{{title}}</b>
    <p *ngFor="let line of content">{{line}}</p>
  `,
  styleUrls: ['./moonday.component.scss']
})
export class MoondayComponent implements OnInit {

  title: string;
  content: string[];

  constructor() {
    const { title, content } = Moon.getInstance().getCurrentMoonDayData();
    this.title = title;
    this.content = content;
  }

  ngOnInit() {
  }

}
