import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'x-header',
  template: `
    <div class="bar">
      <div class="logo"></div>
      <ul class="nav">
        <li *ngFor="let anchor of anchors" class="link-block">
          <a href="#" class="link">{{anchor}}</a>
        </li>
      </ul>
    </div>
  `,
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  anchors: string[];

  constructor() {
    this.anchors = [
      'Главная',
      'Обо мне',
      'Услуги',
      'Контакты'
    ]
  }

  ngOnInit() {
  }

}
