import { Component } from '@angular/core';

@Component({
  selector: 'x-root',
  template: `
    <x-container>
      <x-header></x-header>
      <x-moonday></x-moonday>
    </x-container>
  `
})
export class AppComponent {
}
